# Todo Bash

![](https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/images/todo.icon.svg)


## Information

Simple ToDO List using bash, using terminal app & desktop app.

|Application type | Language |
|--|--|
|![Terminal](https://raw.githubusercontent.com/darealshinji/haiku-icons/fb9aa409eee9287ffc2e5d0477e1329f694ed707/svg/App_Terminal.svg) ![Tracker](https://raw.githubusercontent.com/darealshinji/haiku-icons/fb9aa409eee9287ffc2e5d0477e1329f694ed707/svg/App_Tracker.svg) | ![Terminal](https://raw.githubusercontent.com/darealshinji/haiku-icons/fb9aa409eee9287ffc2e5d0477e1329f694ed707/svg/App_Terminal.svg) |

[Fork](https://github.com/Lateralus138/todo-bash) de este programa de lista de tareas creado en Bash Scripting que utilizo junto con el [script de quotes](https://codeberg.org/_-Caleb-_/Bash_Citas) que incluye lo siguiente respecto a la [versión original de GitHub](https://github.com/Lateralus138/todo-bash)):

- Eliminación de scripts duplicados del repositorio.
- Traducción al español.
- Añadidos algunos colores para la salida en la consola.
- Añadidos nuevos estilos en la lista de tareas haciéndola más amigable.

## Instalación:

Descarga el archivo [instalar.sh](https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/instalar.sh) y ejecútalo desde una Terminal usando "bash instalar.sh".

Sigue las instrucciones y disfruta de ToDO.


![](https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/images/todohaikuterminal.png)

![](https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/images/todomenu.png)

![](https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/images/todohaikufull.png)