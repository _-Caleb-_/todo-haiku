#!/bin/bash

paso2 () {
alert --info "Se han instalado las dependencias necesarias, a continuación se instalará la última versión de ToDO" "Instalar"
cd $HOME/config/non-packaged/bin
wget https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/todo
wget https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/todo_desk
wget https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/todo_desklink
chmod +x todo
chmod +x todo_desk
chmod +x todo_desklink
# Estableciendo Icono.
addattr -t icon -f /boot/home/config/settings/todo.hvif "BEOS:ICON" /boot/home/config/non-packaged/bin/todo
paso3 
}


paso3 () {
p3=$(alert --idea "¿Deseas instalar ToDO en el arranque del sistema?" "NO" "SI")
if [ $p3 = "SI" ]; then
	ln -s  $HOME/config/non-packaged/bin/todo $HOME/config/settings/boot/launch/todo
	alert --info "Enlace simbólico creado"
	paso4
else
	paso4
fi
}

paso4 () {
p4=$(alert --idea "¿Deseas instalar ToDO en la barra de escritorio para añadir y eliminar tareas de forma visual?" "NO" "SI")
if [ $p4 = "SI" ];
	then
	ln -s  $HOME/config/non-packaged/bin/todo_desklink $HOME/config/settings/boot/launch/todo_desklink
	alert --info "Enlace simbólico creado"
	alert --info "Instalación finalizada, reinicia el sistema para probar que todo vaya bien"
 	else
	alert --info "Instalación finalizada, reinicia el sistema para probar que todo vaya bien"
	exit 0
fi
}

Paso1=$(alert --idea "¿Deseas instalar ToDO (Gestor de tareas simple) en el sistema?" "NO" "SI")
if [ $Paso1 = "SI" ]; then
alert --info "Se van a instalar algunas cosas, asegurate de tener conexión a Internet activa."
	cd /boot/home/config/non-packaged/bin
	wget https://codeberg.org/_-Caleb-_/BeInput/raw/branch/main/BeInput
	sleep 3
	chmod +x BeInput
		cd /boot/home/config/settings
	wget https://codeberg.org/_-Caleb-_/todo-haiku/raw/branch/master/images/todo.hvif
	pkgman install yab -y
paso2
else
alert --stop "Cancelado por el usuario"
fi
